Oxygen Not Included - Traduction FR
===================================

Traduction en français pour le jeu *Oxygen Not Included* de l'éditeur Klei Entertainment.

Traductions utilisées pour le mod FR du jeu disponible sur Steam à cette adresse:
[Lien](http://steamcommunity.com/sharedfiles/filedetails/?id=928348692)

Le fichier .pot (PO Template) est fourni par l'éditeur du jeu et contient les chaînes à traduire

Consultez le fichier [changelog.md](changelog.md) pour la liste des modifications.

