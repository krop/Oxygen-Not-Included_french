# Journal des modifications:

v1.64.0 - 30-octobre-2021 (rev. 484114)
--------------------------------
Chaînes traduites: 13300/13300 (100%)

* Traduction des chaînes pour la maj « Rad New Worlds »

v1.63.0 - 8-octobre-2021 (rev. 481350)
--------------------------------
Chaînes traduites: 13235/13235 (100%)

* Traduction des chaînes pour la maj « Unexplained Traits »

v1.62.1 - 16-septembre-2021
--------------------------------
Chaînes traduites: 13135 / 13135 (100%)

* Traduction des chaînes pour la maj « Cosmic Calling »

v1.60.0 - 05-septembre-2021
--------------------------------
Chaînes traduites: 13107 / 13107 (100%)

* Mise à jour des chaînes pour la version
  stable et la version test du DLC.

v1.15.0 - 28-avril-2021 (CS-460672)
--------------------------------
Chaînes traduites: 11832 / 12053 (98%)

* Mise à jour des chaînes pour la version stable 'CS-460672'

Note: Le DLC 'Spaced Out' n'est que très partiellement traduit avec
cette mise à jour.

v1.14.0 - 25-novembre-2020 (CS-442154-D)
--------------------------------
Chaînes traduites: 10324/10324 (100%)

* Traduction et mise à jour des chaînes pour la maj « Cloud Save »

v1.13.3 - 26-octobre-2020 (AP-419840-D)
--------------------------------
Chaînes traduites: 10253/10253 (100%)

* Corrections mineures.

v1.13.2 - 07-juillet-2020 (AP-419840-D)
--------------------------------
Chaînes traduites: 10253/10253 (100%)

* Mise à jour des chaines suite à la mise à jour 419840

v1.13.1 - 06-mai-2020 (AP-408920-D)
--------------------------------
Chaînes traduites: 10236/10236 (100%)

* Mise à jour des chaines suite à la mise à jour 408920

v1.13.0 - 05-mars-2020 (AP-398142-D)
--------------------------------
Chaînes traduites: 10182/10182 (100%)

* Mise à jour des chaines suite à la mise à jour « Banhi's Automation Innovation Pack »

v1.12.2 - 11-février-2020 (RP-394232-D)
--------------------------------
Chaînes traduites: 9921/9921 (100%)

* Mise à jour des chaines suite à la mise à jour 394232

v1.12.1 - 07-décembre-2019 (RP-383949)
--------------------------------
Chaînes traduites: 9911/9911 (100%)

* Mise à jour des chaines suite à la mise à jour 383949

v1.12.0 - 26-novembre-2019 (RP-381897)
--------------------------------
Chaînes traduites: 9893/9893 (100%)

* Traduction des chaines de la mise à jour « Recreation Pack »

v1.11.6 - 06-octobre-2019 (372041)
--------------------------------
Chaînes traduites: 9804/9804 (100%)

* Mise à jour des chaînes suite à la mise à jour 372041

v1.11.5 - 07-septembre-2019 (366134)
--------------------------------
Chaînes traduites: 9772/9772 (100%)

* Mise à jour des chaînes suite à la mise à jour 366134

Modification des traductions:

* Gas shutoff -> Vanne d'arrêt de gaz
* Liquid shutoff -> Vanne d'arrêt de liquide

v1.11.4 - 22-août-2019 (361684)
--------------------------------
Chaînes traduites: 9757/9757 (100%)

* Mise à jour des chaînes suite à la mise à jour 361684

v1.11.3 - 16-août-2019 (359645)
--------------------------------
Chaînes traduites: 9708/9708 (100%)

* Mise à jour des chaînes suite à la mise à jour 359645

v1.11.2 - 10-août-2019 (358820)
--------------------------------
Chaînes traduites: 9701/9701 (100%)

* Mise à jour des chaînes suite à la mise à jour 358820

v1.11.1 - 01-août-2019 (357226)
--------------------------------
Chaînes traduites: 9697/9697 (100%)

* Mise à jour des chaînes pour la mise à jour 357226
* Ajout de la version du mod FR à l'écran

v1.11.0 - 30-juillet-2019 (356355)
--------------------------------
Chaines traduites: 9672/9672 (100%)

Bienvenue et merci d'utiliser le mod FR pour Oxyxen Not Included !

Ce mod est l'aboutissement de très nombreuses heures de travail et nous espérons
qu'il vous permettra d'apprécier encore davantage votre expérience du jeu.

N'hésitez-pas à nous faire part des erreurs que vous pourriez rencontrer en
postant vos remarques dans la section « Discussions » du workshop Steam.


Les changements effectués avec cette mise à jour du mod:

* Traduction des chaines de la mise à jour « Launch Update »
* Suppression d'une grande partie des majuscules en milieu de phrase.

* Nouvelles traductions pour certains équipements :
  - Fontaine à eau -> Débouteilleur
  - Fontaine à Bras -> Pompe à bras
  - Grande salle -> Salle de banquet
  - Colis surprise -> Colis de ravitaillement

* Nouvelle traduction pour certaines jeunes créatures :
  (*uniquement* les créatures juvéniles)
  - Bouffette -> Bouffie
  - Glouton -> Bouftou

* Modification de la traduction pour une créature :
  - Meumeuh à gaz -> Meumeuh

* Nouvelles traductions pour certains menus de construction:
  L'objectif étant, encore une fois, de gagner de la place afin d'améliorer
  la lisibilité.
  - Plomberie -> Liquides
  - Ventilation -> Gaz
  - Fuséologie -> Fusées
  - Automatisme -> Circuits logiques

v1.10.4 - 23-avril-2019 (Q3-327401)
--------------------------------
Chaines traduites: 8822/8822 (100%)

* Mise à jour pour la version Q3-327401

v1.10.3 - 17-avril-2019 (Q3-326830)
--------------------------------
Chaines traduites: 8822/8822 (100%)

* Mise à jour pour la version Q3-326830

v1.10.0 - 16-avril-2019 (Q3-326232)
--------------------------------
Chaines traduites: 8822/8822 (100%)

* Traduction des chaines de la mise à jour « Quality of Life Mark III »
* Modification des adjectifs qualifiant la nourriture :
  - Pauvre -> Mauvais
  - Standard -> Banal
  - Bon -> Goûteux
  - Super -> Savoureux
  - Superbe -> Succulent
  - Terrible -> Insipide

v1.9.1 - 27-février-2019 (Q2-311694)
--------------------------------
Chaines traduites: 8533/8533 (100%)

* Mise à jour de la traduction suite hotfix Q2-311694

v1.9.0 - 19-février-2019 (Q2-309851)
--------------------------------
Chaines traduites: 8530/8531 (100%)

* Traduction des chaines de la mise à jour « Quality of Life Mark II »
* Les noms et adjectifs utilisés pour générer les noms aléatoires des colonies
  sont désormais tous du même genre grammatical.

v1.8.1 - 04-janvier-2019 (Q1-300556)
--------------------------------
Chaines traduites: 8329/8329 (100%)

* Mise à jour des chaînes suite au dernier hotfix
* Corrections et reformulations

v1.8.0 - 13-décembre-2018 (Q1-299745)
--------------------------------
Chaines traduites: 8329/8329 (100%)

* Traduction des chaines de la mise à jour « Quality of Life Mark I »
* Suppression des majuscules superflues pour certaines chaînes
* Remplacement de "Boue" par "Terre" pour éviter les confusions
* Changements de certains noms:
  - Fermier -> Cultivateur
  - Dalle fermière -> Dalle de culture

v1.7.2 - 29-octobre-2018 (SI-291640)
--------------------------------
Chaines traduites: 8023/8023 (100%)

* Mise à jour des chaînes

v1.7.1 - 19-octobre-2018 (SI-290261)
--------------------------------
Chaines traduites: 8019/8019 (100%)

* Mise à jour des chaînes pour le premier hotfix de la MAJ « Space Industry »

v1.7.0 - 18-octobre-2018 (SI-290148)
--------------------------------
Chaines traduites: 8014/8014 (100%)

* Traduction des chaines de la mise à jour « Space Industry »

v1.6.1 - 22-septembre-2018 (RU-285480)
--------------------------------
Chaines traduites: 7758/7758 (100%)

* Mise à jour des chaînes

v1.6.0 - 06-septembre-2018 (RU-284571)
--------------------------------
Chaines traduites: 7758/7758 (100%)

* Traduction des chaines de la mise à jour « Rocketry »

v1.5.3 - 02-août-2018 (EU-280450)
--------------------------------
Chaines traduites: 7367/7367 (100%)

* Mise à jour des chaînes

v1.5.2 - 30-juillet-2018 (EU-279899)
--------------------------------
Chaines traduites: 7355/7355 (100%)

* Mise à jour des chaînes pour le troisième hotfix de la MAJ « Expressive »

v1.5.1 - 26-juillet-2018 (EU-279497)
--------------------------------
Chaines traduites: 7296/7296 (100%)

* Mise à jour des chaînes pour le premier hotfix de la MAJ « Expressive »

v1.5.0 - 26-juillet-2018 (EU-279457)
--------------------------------
Chaines traduites: 7308/7308 (100%)

* Traduction des chaines de la mise à jour « Expressive »
* Changement de noms pour les créatures:
  - Gluant à poils longs -> Gluant Velu

v1.4.0 - 14-juin-2018 (CU-273433)
--------------------------------
Chaines traduites: 6838/6838 (100%)

* Traduction des chaines de la mise à jour « Cosmic »
* Changement de noms pour les constructions pour pallier les problèmes de place
  dans l'interface:
  - Fourniture -> Mobilier
  - Nourriture -> Aliments
  - Médecine -> Soins
  - Automatisation -> Automatisme (terme correct)
  - Utilitaires -> Divers
  - Livraison -> Convoi
* Corrections et reformulations d'anciennes traduction
* Changement de nom pour certaines créatures:
  - Luciole Vitale -> Luciole Énergique
  - Bouffette Dense -> Bouffette Compacte
  - Poisson Gulp -> Poisson Gobeur
  - Hatch -> Glouton

La discussion sur les traductions des noms de créatures se trouve ici:
https://forums.kleientertainment.com/topic/90444-l10n-help-wanted/

v1.3.1 - 05-mai-2018 (R2-266730)
--------------------------------
Chaines traduites: 6697/6697 (100%)

* Corrections et traductions de chaines pour les derniers hotfix de la mise à
  jour « Ranching II »

v1.3.0 - 03-mai-2018 (R2-266730)
--------------------------------
Chaines traduites: 6695/6695 (100%)

* Mise à jour et traduction des chaines de la mise à jour « Ranching II »
* Nouvelles traductions pour les créatures et plantes:
  - Bluff Briar: Fausse Bruyère
  - Muck Roots: Tubercule Boueux
  - Puft: Bouffette

Merci à Hasharin pour ses suggestions.

v1.1.0 - 16-fev-2018 (OC-255486)
--------------------------------
Chaines traduites: 5880/5880 (100%)

* Mise à jour pour le hotfix OC-255486
* Correction de la traduction pour une recette

v1.1.0 - 08-fev-2018
--------------------
Chaines traduites: 5876/5876 (100%)

* Mise à jour et traduction des chaines de la mise à jour « Occupational Upgrade »
* Encore beaucoup de corrections et d'adaptation des chaînes
* La 'Déconstruction' avec un détonateur d'explosifs n'était pas assez violente,
  place à la 'Démolition'
* Remplacement de "Décor" par "Décoration" dans les traductions. Elles sont
  nettement plus jolies ainsi
* Traduction de certaines recettes qui raviront vos papilles.
* Traduction de wheezewort

v1.0.0 - 05-jan-2018
--------------------
Chaînes traduites: 5517/5525 (99.85%)

* Importation des traductions existantes
* Beaucoup, Beaucoup de corrections, reformulations, corrections
